#!/bin/bash -u

# Code written to generate scripts from a template script that run singleCell simulations using a dynamic protocol.

savedir=/home/mzile/Results/AF_alternans/AF/sim_batch0001;
sim_num=1; # simulation number at which to start numbering simulations

sweep_vals=( "25" "50" "75" "100" "125" "150" "175" "200" "225" "250" "275" "300" );
sweep_names=( "factor_perm50" "factor_koffL" "factor_koffH" "factor_kon" "factor_fapp" "factor_gapp" "factor_hf" "factor_hb" "factor_gxb" "factor_kn_p" "factor_kp_n" );
length_sweepN=${#sweep_names[@]};

#CLM: Command line Modifiers
CLM_names=( "stretch-fixed" );
num_stretch_sweep=7;
SL_min=1.65;
SL_max=2.4;
SL_rest=1.9;
SL_value=${SL_max};
CLM_SL_vals=( "1.70" "1.80" "1.90" "2.00" "2.10" "2.20" "2.30" );
for ii in $(seq 0 1 $(( ${#CLM_SL_vals[@]}-1 )) ); do
    CLM_lambda_vals[${ii}]=$( bc -l <<< "(${CLM_SL_vals[ii]}/${SL_rest})" );
done

#ND: non-default parameters
ND_names=( "paramset" "TnCai" "AF" "RA" "factor_kiCa" );
ND_vals=( "2" "2" "1" "1" "0.50" );
length_NDn=${#ND_names[@]};
length_NDv=${#ND_vals[@]};

base=dynamicPacing;

if [[ ${length_NDn} -eq ${length_NDv} ]]; then

	for mm in $( seq 0 1 $(( ${length_sweepN}-1 )) ); do
	    
	    parString="'";
	    for ii in $( seq 0 1 $(( ${length_NDn}-1 )) ); do
		# make parameter string
		parString+="${ND_names[ii]}=${ND_vals[ii]}";
		if [[ ii -lt $(( ${length_NDn}-1 )) ]]; then
		    parString+=",";
		fi
	    done

	    for jj in $( seq 0 1 $(( ${#sweep_vals[@]}-1 )) ); do
		parString_final="${parString},${sweep_names[mm]}=$( bc -l <<< "1+(${sweep_vals[jj]}/100)" )'";
		    for pp in $( seq 0 1 $(( ${num_stretch_sweep}-1)) ); do #loop through CLM values!
				CLMString="";
				CLMString="--${CLM_names[0]}=${CLM_lambda_vals[pp]}";
				echo ${CLMString[@]};

				if [[ sim_num -lt 10 ]]; then
				print_sim_num=000${sim_num};
				elif [[ sim_num -lt 100 ]]; then
				print_sim_num=00${sim_num};
				elif [[ sim_num -lt 1000 ]]; then
				print_sim_num=0${sim_num};
				else
				print_sim_num=${sim_num};
				fi
				simdir=sim${print_sim_num};

				outFile=${base}_sim${print_sim_num}.sh;
				cat ${base}_base.sh \
				| sed 's/++simnum++/'${print_sim_num}'/g' \
				| sed 's/++actpar++/'${parString_final}'/g' \
				| sed 's/++simdir++/'${simdir}'/g' \
				| sed 's#++savedir++#'${savedir}'#g' \
				| sed 's/++CLMstr++/'${CLMString}'/g' \
				>${outFile};

				echo ${outFile};
				echo "parString_final = ${parString_final}";

				echo $( chmod +x ./${outFile} )
				echo $( mkdir ${savedir}/${simdir} )
				echo $( mv ./${outFile} ${savedir}/${simdir}/${outFile} )
	
				(( sim_num++));
		     done
	    done

	done

else

    echo "Need to provide the same number of parameter names (" ${length_NDn} ") as values (" ${length_NDv} ")."

fi


exit 0;
