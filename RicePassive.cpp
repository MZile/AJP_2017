
#include "RicePassive.hpp"
#include "utility.hpp"

#include "using.hpp"

void RicePassive::setDefaultFlags() {}
void RicePassive::setDefaultParameters() {
  pconTitin = 0.002;
  pexpTitin = 10;
}
void RicePassive::init() {}

RicePassive::RicePassive() {
  setDefaultFlags();
  setDefaultParameters();
  init();
}

void RicePassive::stressFromStrain(const FloatData2D& EE, FloatData2D& TT) {
  Real stretch = sqrt(2*EE(0,0)+1);

  TT.fill(0);
  if (stretch >= 1) {
    TT(0,0) =  pconTitin*expm1(pexpTitin*(stretch-1));
  } else {
    TT(0,0) = -pconTitin*expm1(-pexpTitin*(stretch-1));
  }
}

void RicePassive::d_stressFromStrain(const FloatData2D& EE, const FloatData2D& d_EE, FloatData2D& d_TT) {
  Real stretch = sqrt(2*EE(0,0)+1);
  Real d_stretch = 1/(2*stretch)*2*d_EE(0,0);

  d_TT.fill(0);
  if (stretch >= 1) {
    d_TT(0,0) =  pconTitin*exp( pexpTitin*(stretch-1))*pexpTitin*d_stretch;
  } else {
    d_TT(0,0) =  pconTitin*exp(-pexpTitin*(stretch-1))*pexpTitin*d_stretch;
  }
}

void RicePassive::serialize(IArchive& unused_p) {
  UNUSED(p);
}
