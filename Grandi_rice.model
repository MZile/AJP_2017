//---------------------------------------------- Ionic Model ---------------------------------------------------------------------

/* Initial conditions  */
Vm_init        = -74.5644;
//CaM_init     = 0.000744434;
Ca_sr_init     = 0.508903;
Cai_init       = 0.000223811;
Caj_init       = 0.000345452;
Casl_init      = 0.000248308;
//Csqnb_init   = 1.14172;
Myoc_init      = 0.00440683;
Myom_init      = 0.135077;
//NaBj_init    = 3.72627;
//NaBsl_init   = 0.813117;
Nai_init       = 9.71245;
Naj_init       = 9.71153;
Nasl_init      = 9.71224;
RyRi_init      = 6.23409e-07;
RyRo_init      = 2.25496e-06;
RyRr_init      = 0.783413;
//SLHj_init    = 0.106621;
//SLHsl_init   = 0.197824;
//SLLj_init    = 0.014361;
//SLLsl_init   = 0.0227865;
//SRB_init     = 0.00465802;
TnCHc_init     = 0.129041;
TnCHm_init     = 0.00513294;
TnCL_init      = 0.0192022;
d_init         = 1.79644e-05;
f_init         = 0.998608;
fcaBj_init     = 0.0479864;
fcaBsl_init    = 0.0354449;
h_init         = 0.859667;
hl_init        = 0.0407412;
j_init         = 0.878584;
m_init         = 0.00851597;
ml_init        = 0.00851597;
rkur_init      = 0.000345402;
skur_init      = 0.947686;
xkr_init       = 0.00335534;
xks_init       = 0.00692895;
xtof_init      = 0.00124505;
ytof_init      = 0.950582;

/* Model Parameters */
group {
  AF    = 0;  	            //= 1 for cAF, =0 for sinus rhythm
  RA    = 0;                //= 1 for RA, =0 for LA
  add_I_NaL         = 0;    // add late Na current
  usefca = 0;

} .flag();
  
group {
  factor_g_CaL      = 1.0;  // scale L-type calcium channel current (changes pCa, pNa, and pK)
  factor_Vmax_SRCaP = 1.0;  // scale SR uptake
  factor_J_SRleak   = 1.0;  // scale SR leak
  factor_J_SRCarel  = 1.0;  // scale SR release

  factor_g_K1       = 1.0;  // scale I_K1 current
  factor_g_Na       = 1.0;  // scale I_Na current
  factor_g_NaL      = 1.0;  // scale I_NaL current
  factor_g_Kr       = 1.0;  // scale I_Kr current
  factor_g_Ks       = 1.0;  // scale I_Ks current
  factor_g_Kur      = 1.0;  // scale I_Kur current
  factor_g_to       = 1.0;  // scale I_to current
  factor_Ibar_NCX   = 1.0;  // scale I_NCX current

  factor_ec50SR     = 1.0;  // scale ec50_SR
  factor_Kmf        = 1.0;  // scale Km of SERCA forward mode
  factor_Kmr        = 1.0;  // scale Km of SERCA reverse mode
  factor_tau_f      = 1.0;  // scale tau of voltage-dependent inactivation gate of L-type calcium channel
  factor_tau_fCa    = 1.0;  // scale tau of Ca-dependent inactivation gate of L-type calcium channel

  factor_kim        = 1.0;  // scale kim
  factor_kom        = 1.0;  // scale kom
  factor_kiCa       = 1.0;  // scale kiCa
  factor_koCa       = 1.0;  // scale koCa
  factor_MaxSR      = 1.0;  // scale MaxSR
  factor_MinSR      = 1.0;  // scale MinSR
  
  value_MaxSR       = 15.0; // set MaxSR
  value_MinSR       = 1.0;  // set MinSR

} .flag();

/* Numerical Methods */
group {
  RyRr;
  RyRo;
  RyRi;
} .ub(1); .lb(0); .method(markov_be);


group {
  m;
  h;
  j;
  d;
  f;
  fcaBj;
  fcaBsl;
  xtof;
  ytof;
  xkr;
  xks;
  rkur;
  skur;
  ml;
  hl;
} .ub(1); .lb(0);

group {
   TnCL;
   TnCHc;
   TnCHm;
   Myoc;
   Myom;
   Ca_sr;
   Naj;
   Nasl;
   Nai;
   Caj;
   Casl;
   Cai;
} .lb(1e-10);


Vm; //.external(Vm); //.nodal();


/* Tracing Currents, etc. */
group {
  Vm;                                                 //transmembrane potential
  I_Na;                                               //sodium
  I_NaL;                                              //late sodium component
  I_nabk;                                             //NA Background Current
  I_nak;                                              //Na/K Pump Current
  I_kr;                                               //Rapidly activating K current
  I_ks;                                               //slow activating K current
  I_kp;                                               //Plateau K current
  I_kur;                                              //Ultra rapid delayed rectifier Outward K current
  I_to;                                               //Transient Outward K current (slow and fast component)
  I_ki;                                               //time-independent K Current
  I_Ca;                                               //L type Calcium current
  I_ClCa;                                             //Ca-activated Cl current
  I_Clbk;                                             //background Cl current
  I_ncx;                                              //NA/Ca exchanger flux
  I_pca;                                              //Sarcolemmal Ca Pump Current
  I_cabk;                                             //Ca Background Current
  RyRr;
  RyRo;
  RyRi;
  Ca_sr;
  Caj;
  Casl;
  Cai;
} .trace();

/* Constants */
pi   = 3.14159;
R    = 8314;                                           // [J/kmol*K]  
Frdy = 96485;                                          // [C/mol]  
Temp = 310;                                            // [K]
FoRT = Frdy/R/Temp;
Cmem = 1.1e-10;                                        // [F] membrane capacitance; ventricular value = 1.3810e-10;
Qpow = (Temp-310)/10;

/* Cell geometry */
cellLength = 100;                                      // cell length [um]
cellRadius = 10.25;                                    // cell radius [um]
Vcell = pi*cellRadius*cellRadius*cellLength*1e-15;     // [L]
Vmyo = 0.65*Vcell;
Vsr = 0.035*Vcell;
Vsl = 0.02*Vcell;
Vjunc = 0.000539*Vcell;
J_ca_juncsl = 1/1.2134e12;                             // [L/msec] = 8.2413e-13
J_ca_slmyo = 1/2.68510e11;                             // [L/msec] = 3.2743e-12
J_na_juncsl = 1/(1.6382e12/3*100);                     // [L/msec] = 6.1043e-13
J_na_slmyo = 1/(1.8308e10/3*100);                      // [L/msec] = 5.4621e-11

/* Fractional currents in compartments */
Fjunc = 0.11;
Fsl = 1-Fjunc;
Fjunc_CaL = 0.9;
Fsl_CaL = 1-Fjunc_CaL;

/* Fixed ion concentrations */
Cli = 15;            				       // Intracellular Cl  [mM]
Clo = 150;           				       // Extracellular Cl  [mM]
Ko  = 5.4;           				       // Extracellular K   [mM]
Nao = 140;           				       // Extracellular Na  [mM]
Cao = 1.8;           				       // Extracellular Ca  [mM]
Mgi = 1;             				       // Intracellular Mg  [mM]
Ki  = 120;           				       // Intracellular K   [mM]

/* Nernst Potentials */
ena_junc = (1/FoRT)*log(Nao/Naj);                      // [mV]
ena_sl = (1/FoRT)*log(Nao/Nasl);      		       // [mV]
ek = (1/FoRT)*log(Ko/Ki);	       		       // [mV]
eca_junc = (1/FoRT/2)*log(Cao/Caj);    	   	       // [mV]
eca_sl = (1/FoRT/2)*log(Cao/Casl);    		       // [mV]
ecl = (1/FoRT)*log(Cli/Clo);           		       // [mV]

/* Na transport parameters */
GNa = factor_g_Na*14*(1-0.1*AF);       	 	       // [mS/uF]
GNaB = 0.597e-3;                       		       // [mS/uF]
IbarNaK = 0.7*1.8;//1.90719;           		       // [uA/uF]
KmNaip = 11*(1-0.25*ISO);              		       // [mM]
KmKo = 1.5;                            		       // [mM]
Q10NaK = 1.63;  
Q10KmNai = 1.39;


if (AF==1 or factor_g_NaL != 1 or add_I_NaL==1) {
  GNaL = 0.0025*factor_g_NaL;
} else {
  GNaL = 0;
}

/* K current parameters */
pNaK = 0.01833;      
gkp = 2*0.001;

/* I_to parameters */
GtoFast = factor_g_to*(1-0.45*AF*(1-RA)-0.8*AF*RA)*(1+(0.28*CT-0.32*APG))*0.165; //nS/pF

/* Cl current parameters */
GClCa = 0.5*0.109625;                             // [mS/uF]
GClB = 9.0e-3;                                    // [mS/uF]
KdClCa = 100e-3;                                  // [mM]

/* I_Ca parameters */
pNa = factor_g_CaL*(1+0.5*ISO)*(1-0.5*AF)*(1+(0.6*CT-0.04*PM+0.01*APG-0.36*AVR))*0.50*1.5e-8;       // [cm/sec]
pCa = factor_g_CaL*(1+0.5*ISO)*(1-0.5*AF)*(1+(0.6*CT-0.04*PM+0.01*APG-0.36*AVR))*0.50*5.4e-4;       // [cm/sec]
pK  = factor_g_CaL*(1+0.5*ISO)*(1-0.5*AF)*(1+(0.6*CT-0.04*PM+0.01*APG-0.36*AVR))*0.50*2.7e-7;       // [cm/sec]
Q10CaL = 1.8;

/* Ca transport parameters */
IbarNCX = factor_Ibar_NCX*(1+0.4*AF)*0.7*4.5;     // [uA/uF]
KmCai = 3.59e-3;                                  // [mM]
KmCao = 1.3;                                      // [mM]
KmNai = 12.29;                                    // [mM]
KmNao = 87.5;                                     // [mM]
ksat = 0.27;                                      // [none]  
nu = 0.35;                                        // [none]
Kdact =1.5*0.256e-3;                              // [mM] 
Q10NCX = 1.57;                                    // [none]
IbarSLCaP =  0.7*0.0673;                          // [uA/uF]
KmPCa =0.5e-3;                                    // [mM] 
GCaB = 1.10*5.513e-4;                             // [uA/uF]
Q10SLCaP = 2.35;                                  // [none]

/* SR flux parameters */
Q10SRCaP = 2.6;                                   // [none]
Vmax_SRCaP = factor_Vmax_SRCaP*5.3114e-3;         // [mM/msec]
Kmf = (2.5-1.25*ISO)*factor_Kmf*0.246e-3;         // [mM]
Kmr = factor_Kmr*1.7;                             // [mM]
hillSRCaP = 1.787;                                // [mM]
ks = factor_J_SRCarel*25;                         // [1/ms]      
koCa = factor_koCa*(10+20*AF+10*ISO*(1-AF));      // [mM^-2 1/ms]
kom = factor_kom*0.06;                            // [1/ms]     
kiCa = factor_kiCa*0.5;                           // [1/mM/ms]
kim = factor_kim*0.005;                           // [1/ms]
ec50SR = factor_ec50SR*0.45;                      // [mM]
K_SRleak = factor_J_SRleak*(1+0.25*AF)*5.348e-6;  // [1/ms]

/* Buffering parameters */
Bmax_Naj = 7.561;                                 // [mM]                     
Bmax_Nasl = 1.65;                                 // [mM]
koff_na = 1e-3;                                   // [1/ms]
kon_na = 0.1e-3;                                  // [1/mM/ms]
Bmax_TnClow = 70e-3;                              // [mM] 
koff_tncl = (1+0.5*ISO)*19.6e-3;                  // [1/ms] 
kon_tncl = 32.7;                                  // [1/mM/ms]
Bmax_TnChigh = 140e-3;                            // [mM
koff_tnchca = 0.032e-3; 	                  // [1/ms] 
kon_tnchca = 2.37;                                // [1/mM/ms]
koff_tnchmg = 3.33e-3;                            // [1/ms] 
kon_tnchmg = 3e-3;                                // [1/mM/ms]
Bmax_CaM = 24e-3;                                 // [mM]
koff_cam = 238e-3;                                // [1/ms] 
kon_cam = 34;                                     // [1/mM/ms]
Bmax_myosin = 140e-3;                             // [mM]  
koff_myoca = 0.46e-3;                             // [1/ms]
kon_myoca = 13.8;                                 // [1/mM/ms]
koff_myomg = 0.057e-3;                            // [1/ms]
kon_myomg = 0.0157;                               // [1/mM/ms]
Bmax_SR = 19*.9e-3;                               // [mM] 
koff_sr = 60e-3;                                  // [1/ms]
kon_sr = 100;                                     // [1/mM/ms]
Bmax_SLlowsl = 37.4e-3*Vmyo/Vsl;                  // [mM]    
Bmax_SLlowj = 4.6e-3*Vmyo/Vjunc*0.1;              // [mM] 
koff_sll = 1300e-3;                               // [1/ms]
kon_sll = 100;                                    // [1/mM/ms]
Bmax_SLhighsl = 13.4e-3*Vmyo/Vsl;                 // [mM] 
Bmax_SLhighj = 1.65e-3*Vmyo/Vjunc*0.1;            // [mM]  
koff_slh = 30e-3;                                 // [1/ms]
kon_slh = 100;                                    // [1/mM/ms]
Bmax_Csqn = 140e-3*Vmyo/Vsr;                      // [mM] 
koff_csqn = 65;                                   // [1/ms] 
kon_csqn = 100;                                   // [1/mM/ms] 


			/* Membrane Currents */

/* I_Na: Fast Na currnet */
if ( Vm==-47.13 ) {
  a_m = 3.2;
} else {
  a_m = 0.32*(Vm+47.13)/(1-exp(-0.1*(Vm+47.13)));
}
b_m = 0.08 * exp( -(Vm)/11. );

if ( Vm >= -40.) {
  a_h = 0.0;
} else {
  a_h = (0.135*exp( (Vm+80)/-6.8 ));
}

if ( Vm >= -40. ) {
  b_h = (1./0.13/(1+exp(-(Vm+10.66)/11.1)));
} else {
  b_h = (3.56*exp(0.079*Vm)+3.1e5*exp(0.35*Vm));
}

if ( Vm < -40 ) {
  a_j = (-127140*exp(0.2444*Vm)-3.474e-5*exp(-0.04391*Vm))*(Vm+37.78)/
    (1+exp(0.311*(Vm+79.23)));
} else {
   a_j = 0.0;
}

if ( Vm >= -40. ) {
  b_j = 0.3*exp(-2.535e-7*Vm)/(1+exp(-0.1*(Vm+32)));
} else {
  b_j = 0.1212*exp(-0.01052*Vm)/(1+exp(-0.1378*(Vm+40.14)));
}

I_Na_junc = Fjunc*GNa*m*m*m*h*j*(Vm-ena_junc);
I_Na_sl = Fsl*GNa*m*m*m*h*j*(Vm-ena_sl);
I_Na = I_Na_junc+I_Na_sl;


/* I_NaL: Late Na current */
if ( Vm == -47.13 ) {
  a_ml = 3.2;                                                     
} else {
  a_ml = 0.32*(Vm+47.13)/(1-exp(-0.1*(Vm+47.13)));
}
b_ml = 0.08*exp(-Vm/11);
hl_inf = 1/(1+exp((Vm+91)/6.1));
tau_hl=600;

I_NaL_junc = Fjunc*GNaL*ml*ml*ml*hl*(Vm-ena_junc);
I_NaL_sl = Fsl*GNaL*ml*ml*ml*hl*(Vm-ena_sl);
I_NaL = I_NaL_junc + I_NaL_sl;


/* I_nabk: Na Background Current */
I_nabk_junc = Fjunc*GNaB*(Vm-ena_junc);
I_nabk_sl = Fsl*GNaB*(Vm-ena_sl);
I_nabk = I_nabk_junc+I_nabk_sl;

/* I_nak: Na/K Pump Current */
sigma = (exp(Nao/67.3)-1)/7;
fnak = 1/(1+0.1245*exp(-0.1*Vm*FoRT)+0.0365*sigma*exp(-Vm*FoRT));
I_nak_junc = Fjunc*IbarNaK*fnak*Ko /(1+(KmNaip/Naj)*(KmNaip/Naj)*(KmNaip/Naj)*(KmNaip/Naj)) /(Ko+KmKo);
I_nak_sl = Fsl*IbarNaK*fnak*Ko /(1+(KmNaip/Nasl)*(KmNaip/Nasl)*(KmNaip/Nasl)*(KmNaip/Nasl)) /(Ko+KmKo);
I_nak = I_nak_junc+I_nak_sl;

/* I_kr: Rapidly Activating K Current */
gkr = factor_g_Kr*(1+0.53*AVR)*0.035*sqrt(Ko/5.4);
xkr_inf = 1/(1+exp(-(Vm+10)/5));
rkr = 1/(1+exp((Vm+74)/24));
I_kr = gkr*xkr*rkr*(Vm-ek);

/* I_ks: Slowly Activating K Current */ 
eks = (1/FoRT)*log((Ko+pNaK*Nao)/(Ki+pNaK*Nai));
xks_inf = 1 / (1+exp(-(Vm+40*ISO + 3.8)/14.25));                    
tau_xks=990.1/(1+exp(-(Vm+40*ISO+2.436)/14.12));
gks_junc = factor_g_Ks*(1+1*AF+2*ISO)*0.0035;
gks_sl = factor_g_Ks*(1+1*AF+2*ISO)*0.0035;                         
I_ks_junc = Fjunc*gks_junc*xks*xks*(Vm-eks);
I_ks_sl = Fsl*gks_sl*xks*xks*(Vm-eks);                                                                                                                
I_ks = I_ks_junc+I_ks_sl;


/* I_kp: Plateau K current */
kp_kp = 1/(1+exp(7.488-Vm/5.98));
I_kp_junc = Fjunc*gkp*kp_kp*(Vm-ek);
I_kp_sl = Fsl*gkp*kp_kp*(Vm-ek);
I_kp = I_kp_junc+I_kp_sl;

/* I_to: Transient Outward K Current (slow and fast components) */
/* atrium                                                       */
/* equations for activation;                                    */
xtof_inf = ( 1 / ( 1 + exp( -(Vm+1.0)/11.0 ) ) );
tau_xtof = 3.5*exp(-(pow((Vm/30.0),2)))+1.5;

/* equations for inactivation; */
ytof_inf = ( 1 / ( 1 + exp( (Vm+40.5)/11.5) ) ) ;
tau_ytof =25.635*exp(-(pow(((Vm+52.45)/15.8827),2)))+24.14; //14.14
I_tof = 1*GtoFast*xtof*ytof*(Vm-ek);
I_to = 1*I_tof;

/* I_kur: Ultra rapid delayed rectifier Outward K Current */
/* Equation for IKur                                      */
/* atrium                                                 */
/* equations for activation;                              */
Gkur = factor_g_Kur*(1.0-0.45*AF*(1-RA)-0.55*AF*RA)*(1+2*ISO)*0.045*(1+0.2*RA);    //nS/pF maleckar 0.045
rkur_inf = ( 1 / ( 1 + exp( (Vm+6)/-8.6 ) ) );
tau_rkur = 9/(1+exp((Vm+5)/12.0))+0.5;

/* equations for inactivation; */
skur_inf = ( 1 / ( 1 + exp( (Vm+7.5)/10 ) ) );
tau_skur = 590/(1+exp((Vm+60)/10.0))+3050;
I_kur = 1*Gkur*rkur*skur*(Vm-ek);

/* I_ki: Time-Independent K Current */
aki = 1.02/(1+exp(0.2385*(Vm-ek-59.215)));
bki =(0.49124*exp(0.08032*(Vm+5.476-ek)) + exp(0.06175*(Vm-ek-594.31))) /(1 + exp(-0.5143*(Vm-ek+4.753)));
kiss = aki/(aki+bki);

//resting potential
I_ki = factor_g_K1*(1+1*AF)*0.15*0.35*sqrt(Ko/5.4)*kiss*(Vm-ek);

/* I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current */
I_ClCa_junc = Fjunc*GClCa/(1+KdClCa/Caj)*(Vm-ecl);
I_ClCa_sl = Fsl*GClCa/(1+KdClCa/Casl)*(Vm-ecl);
I_ClCa = I_ClCa_junc+I_ClCa_sl;
I_Clbk = GClB*(Vm-ecl);

GClCFTR=0;                                                                        //[mS/uF]
I_ClCFTR = GClCFTR*(Vm-ecl);

/* I_Ca: L-type Calcium Current */
d_inf = 1/(1+exp(-(Vm+3*ISO+9)/6));                                              
if ((Vm == -9 and ISO == 0) or (Vm == -12 and ISO == 1)) {
  tau_d = 1/6/0.035/2; 
} else {
  tau_d = d_inf*(1-exp(-(Vm+3*ISO+9)/6))/(0.035*(Vm+3*ISO+9)); 
}
f_inf = 1/(1+exp((Vm+3*ISO+30)/7))+0.2/(1+exp((50-Vm-3*ISO)/20));                 
tau_f = factor_tau_f*1/(0.0197*exp( -pow((0.0337*(Vm+3*ISO+25)),2) )+0.02);

a_fcaBj =  1.7*Caj/factor_tau_fCa;
b_fcaBj =  11.9e-3/factor_tau_fCa;
a_fcaBsl =1.7*Casl/factor_tau_fCa;
b_fcaBsl = 11.9e-3/factor_tau_fCa;

if (usefca==1) {
  fcaCaMSL= 0.1/(1+(0.01/Casl));
  fcaCaj= 0.1/(1+(0.01/Caj));
} else {
  fcaCaMSL=0;
  fcaCaj= 0;
}

if (Vm == 0){                                                                     
  ibarca_j = pCa*2*Frdy*0.341*(Caj-Cao);
  ibarca_sl = pCa*2*Frdy*0.341*(Casl-Cao);
  ibark = pK*Frdy*0.75*(Ki-Ko);
  ibarna_j = pNa*Frdy*0.75*(Naj-Nao);
  ibarna_sl = pNa*Frdy*0.75*(Nasl-Nao);
} else{
  ibarca_j = pCa*4*(Vm*Frdy*FoRT) * (0.341*Caj*exp(2*Vm*FoRT)-0.341*Cao) /(exp(2*Vm*FoRT)-1);
  ibarca_sl = pCa*4*(Vm*Frdy*FoRT) * (0.341*Casl*exp(2*Vm*FoRT)-0.341*Cao) /(exp(2*Vm*FoRT)-1);
  ibark = pK*(Vm*Frdy*FoRT)*(0.75*Ki*exp(Vm*FoRT)-0.75*Ko) /(exp(Vm*FoRT)-1);
  ibarna_j = pNa*(Vm*Frdy*FoRT)*(0.75*Naj*exp(Vm*FoRT)-0.75*Nao)  /(exp(Vm*FoRT)-1);
  ibarna_sl = pNa*(Vm*Frdy*FoRT)*(0.75*Nasl*exp(Vm*FoRT)-0.75*Nao)  /(exp(Vm*FoRT)-1);
}
I_Ca_junc = (Fjunc_CaL*ibarca_j*d*f*((1-fcaBj)+fcaCaj)*pow(Q10CaL,Qpow))*0.45*1;
I_Ca_sl = (Fsl_CaL*ibarca_sl*d*f*((1-fcaBsl)+fcaCaMSL)*pow(Q10CaL,Qpow))*0.45*1;
I_Ca = I_Ca_junc+I_Ca_sl;
I_CaK = (ibark*d*f*(Fjunc_CaL*(fcaCaj+(1-fcaBj))+Fsl_CaL*(fcaCaMSL+(1-fcaBsl)))*pow(Q10CaL,Qpow))*0.45*1;
I_CaNa_junc = (Fjunc_CaL*ibarna_j*d*f*((1-fcaBj)+fcaCaj)*pow(Q10CaL,Qpow))*0.45*1;
I_CaNa_sl = (Fsl_CaL*ibarna_sl*d*f*((1-fcaBsl)+fcaCaMSL)*pow(Q10CaL,Qpow))*.45*1;
I_CaNa = I_CaNa_junc+I_CaNa_sl;
I_Catot = I_Ca+I_CaK+I_CaNa;

/* I_ncx: Na/Ca Exchanger flux */
Ka_junc = 1/(1+(Kdact/Caj)*(Kdact/Caj));
Ka_sl = 1/(1+(Kdact/Casl)*(Kdact/Casl));
s1_junc = exp(nu*Vm*FoRT)*Naj*Naj*Naj*Cao;
s1_sl = exp(nu*Vm*FoRT)*Nasl*Nasl*Nasl*Cao;
s2_junc = exp((nu-1)*Vm*FoRT)*Nao*Nao*Nao*Caj;
s3_junc = KmCai*Nao*Nao*Nao*(1+(Naj/KmNai)*(Naj/KmNai)*(Naj/KmNai)) + KmNao*KmNao*KmNao*Caj*(1+Caj/KmCai)+KmCao*Naj*Naj*Naj+Naj*Naj*Naj*Cao+Nao*Nao*Nao*Caj;
s2_sl = exp((nu-1)*Vm*FoRT)*Nao*Nao*Nao*Casl;
s3_sl = KmCai*Nao*Nao*Nao*(1+(Nasl/KmNai)*(Nasl/KmNai)*(Nasl/KmNai)) + KmNao*KmNao*KmNao*Casl*(1+Casl/KmCai)+KmCao*Nasl*Nasl*Nasl+Nasl*Nasl*Nasl*Cao+Nao*Nao*Nao*Casl;

I_ncx_junc = Fjunc*IbarNCX*pow(Q10NCX,Qpow)*Ka_junc*(s1_junc-s2_junc)/s3_junc/(1+ksat*exp((nu-1)*Vm*FoRT));
I_ncx_sl = Fsl*IbarNCX*pow(Q10NCX,Qpow)*Ka_sl*(s1_sl-s2_sl)/s3_sl/(1+ksat*exp((nu-1)*Vm*FoRT));
I_ncx = I_ncx_junc+I_ncx_sl;

/* I_pca: Sarcolemmal Ca Pump Current */
I_pca_junc = Fjunc*pow(Q10SLCaP,Qpow)*IbarSLCaP*pow(Caj,1.6)/(pow(KmPCa,1.6)+pow(Caj,1.6));
I_pca_sl = Fsl*pow(Q10SLCaP,Qpow)*IbarSLCaP*pow(Casl,1.6)/(pow(KmPCa,1.6)+pow(Casl,1.6));
I_pca = I_pca_junc+I_pca_sl;

/* I_cabk: Ca Background Current */
I_cabk_junc = Fjunc*GCaB*(Vm-eca_junc);
I_cabk_sl = Fsl*GCaB*(Vm-eca_sl);
I_cabk = I_cabk_junc+I_cabk_sl;

/* SR fluxes: Calcium Release, SR Ca pump, SR Ca leak */
MaxSR = factor_MaxSR*value_MaxSR; MinSR = factor_MinSR*value_MinSR;
kCaSR = MaxSR - (MaxSR-MinSR)/(1+pow((ec50SR/Ca_sr),2.5));
koSRCa = koCa/kCaSR;
kiSRCa = kiCa*kCaSR;
RI = 1-RyRr-RyRo-RyRi;
diff_RyRr = (kim*RI-kiSRCa*Caj*RyRr)-(koSRCa*Caj*Caj*RyRr-kom*RyRo);                 // R
diff_RyRo = (koSRCa*Caj*Caj*RyRr-kom*RyRo)-(kiSRCa*Caj*RyRo-kim*RyRi);               // O
diff_RyRi = (kiSRCa*Caj*RyRo-kim*RyRi)-(kom*RyRi-koSRCa*Caj*Caj*RI);                 // I
J_SRCarel = ks*RyRo*(Ca_sr-Caj);                                                     // [mM/ms]
J_serca = 1*pow(Q10SRCaP,Qpow)*Vmax_SRCaP*(pow((Cai/Kmf),hillSRCaP)-pow((Ca_sr/Kmr),hillSRCaP)) /(1+pow((Cai/Kmf),hillSRCaP)+pow((Ca_sr/Kmr),hillSRCaP)); 
J_SRleak = K_SRleak*(Ca_sr-Caj);                                                     //   [mM/ms]

/* Sodium and Calcium Buffering */
NaBj_term = Bmax_Naj*koff_na/kon_na/((koff_na/kon_na+Naj)*(koff_na/kon_na+Naj));     // NaBj      [mM/ms
beta_Naj = 1/(1+NaBj_term);
NaBsl_term = Bmax_Nasl*koff_na/kon_na/((koff_na/kon_na+Nasl)*(koff_na/kon_na+Nasl)); // NaBsl     [mM/ms]
beta_Nasl = 1/(1+NaBsl_term);

/* Cytosolic Ca Buffers */
diff_TnCL = kon_tncl*Cai*(Bmax_TnClow-TnCL)-koff_tncl*TnCL;                          // TnCL      [mM/ms]
diff_TnCHc = kon_tnchca*Cai*(Bmax_TnChigh-TnCHc-TnCHm)-koff_tnchca*TnCHc;            // TnCHc     [mM/ms]
diff_TnCHm = kon_tnchmg*Mgi*(Bmax_TnChigh-TnCHc-TnCHm)-koff_tnchmg*TnCHm;            // TnCHm     [mM/ms]
CaM_term = Bmax_CaM*koff_cam/kon_cam/((koff_cam/kon_cam+Cai)*(koff_cam/kon_cam+Cai));// CaM       [mM/ms
diff_Myoc = kon_myoca*Cai*(Bmax_myosin-Myoc-Myom)-koff_myoca*Myoc;                   // Myosin_ca [mM/ms]
diff_Myom = kon_myomg*Mgi*(Bmax_myosin-Myoc-Myom)-koff_myomg*Myom;                   // Myosin_mg [mM/ms]
SRB_term = Bmax_SR*koff_sr/kon_sr/((koff_sr/kon_sr+Cai)*(koff_sr/kon_sr+Cai));       // SRB       [mM/ms]
                                                
if (TnCai == TROP_RICE) {
  J_CaB_cytosol = diff_Trop_app_Ca + diff_TnCHc + diff_Myoc;   
} elif (TnCai == TROP_GRANDI) {
  J_CaB_cytosol = diff_TnCL + diff_TnCHc + diff_Myoc;
} else {
  J_CaB_cytosol = diff_TnCHc + diff_Myoc;                                      
}

beta_cytosol = 1/(1+CaM_term+SRB_term);

/* Junctional and SL Ca Buffers *//
SLLj_term = Bmax_SLlowj*koff_sll/kon_sll/((koff_sll/kon_sll+Caj)*(koff_sll/kon_sll+Caj));     // SLLj      [mM/ms]
SLLsl_term = Bmax_SLlowsl*koff_sll/kon_sll/((koff_sll/kon_sll+Casl)*(koff_sll/kon_sll+Casl)); // SLLsl     [mM/ms]
SLHj_term = Bmax_SLhighj*koff_slh/kon_slh/((koff_slh/kon_slh+Caj)*(koff_slh/kon_slh+Caj));    // SLHj      [mM/ms] 
SLHsl_term = Bmax_SLhighsl*koff_slh/kon_slh/((koff_slh/kon_slh+Casl)*(koff_slh/kon_slh+Casl));// SLHsl     [mM/ms]
beta_junction = 1/(1+SLLj_term+SLHj_term);
beta_sl = 1/(1+SLLsl_term+SLHsl_term);

/* Ion concentrations   */
/* SR Ca Concentrations */
Csqnb_term = Bmax_Csqn*koff_csqn/kon_csqn/((koff_csqn/kon_csqn+Ca_sr)*(koff_csqn/kon_csqn+Ca_sr)); // Csqn      [mM/ms]
beta_Ca_sr = 1/(1+Csqnb_term);
diff_Ca_sr = beta_Ca_sr*(J_serca-(J_SRleak*Vmyo/Vsr+J_SRCarel));                          	   // Ca_sr     [mM/ms] 

/* Sodium Concentrations */
I_Na_tot_junc = I_Na_junc+I_nabk_junc+3*I_ncx_junc+3*I_nak_junc+I_CaNa_junc+I_NaL_junc;   // [uA/uF]
I_Na_tot_sl = I_Na_sl+I_nabk_sl+3*I_ncx_sl+3*I_nak_sl+I_CaNa_sl+I_NaL_sl;                 // [uA/uF]

diff_Naj = beta_Naj*(-I_Na_tot_junc*Cmem/(Vjunc*Frdy)+J_na_juncsl/Vjunc*(Nasl-Naj));
diff_Nasl = beta_Nasl*(-I_Na_tot_sl*Cmem/(Vsl*Frdy)+J_na_juncsl/Vsl*(Naj-Nasl) + J_na_slmyo/Vsl*(Nai-Nasl));
diff_Nai = J_na_slmyo/Vmyo*(Nasl-Nai);                                                    // [mM/msec] 

/* Potassium Concentration */
I_K_tot = I_to+I_kr+I_ks+I_ki-2*I_nak+I_CaK+I_kp+I_kur;                                   // [uA/uF]

/* Calcium Concentrations */
I_Ca_tot_junc = I_Ca_junc+I_cabk_junc+I_pca_junc-2*I_ncx_junc;                            // [uA/uF]
I_Ca_tot_sl = I_Ca_sl+I_cabk_sl+I_pca_sl-2*I_ncx_sl;                                      // [uA/uF]
diff_Caj = beta_junction*(-I_Ca_tot_junc*Cmem/(Vjunc*2*Frdy)+J_ca_juncsl/Vjunc*(Casl-Caj)+(J_SRCarel)*Vsr/Vjunc+J_SRleak*Vmyo/Vjunc);   // Ca_j
diff_Casl = beta_sl*(-I_Ca_tot_sl*Cmem/(Vsl*2*Frdy)+J_ca_juncsl/Vsl*(Caj-Casl)+ J_ca_slmyo/Vsl*(Cai-Casl));                             // Ca_sl
diff_Cai = beta_cytosol*(-J_serca*Vsr/Vmyo-J_CaB_cytosol +J_ca_slmyo/Vmyo*(Casl-Cai));

/* Membrane Potential */
I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;                                                   // [uA/uF]
I_Cl_tot = I_ClCa+I_Clbk+I_ClCFTR;                                                        // [uA/uF]
I_Ca_tot = I_Ca_tot_junc+I_Ca_tot_sl;
I_tot = I_Na_tot+I_Cl_tot+I_Ca_tot+I_K_tot;
diff_Vm = -(I_tot)+Istim;

Istim=0;
if (t < 1) {
  Istim=40;                                                                               
}


//--------------------------------------------------------- Myofilament Model  --------------------------------------------------------------------------------                           

lambda; .external();
stretchVel; .external();
tension; .external();

/* Parameters */
EXACT=0;           
OLIVEIRA=2;         
paramset = EXACT; .flag();
RICE2008 = 0;      
riceYear = RICE2008; .flag(); 
NOTROP = 0;
TROP_RICE = 1;
TROP_GRANDI = 2;
TnCai = TROP_RICE; .flag();

group {
  factor_kxb    = 1.0;  // scale kxb
  factor_fapp   = 1.0;  // scale fapp
  factor_gapp   = 1.0;  // scale gapp
  factor_hf     = 1.0;  // scale hf
  factor_hb     = 1.0;  // scale hb
  factor_gxb    = 1.0;  // scale gxb
  factor_kn_p   = 1.0;  // scale kn_p
  factor_kp_n   = 1.0;  // scale kp_n
  factor_kon    = 1.0;  // scale kon
  factor_koffH  = 1.0;  // scale koffH
  factor_koffL  = 1.0;  // scale koffL
  factor_perm50 = 1.0;  // scale perm50
  factor_xbmod  = 1.0;  // scale xbmod
  factor_xbm    = 1.0;  // scale xbm
  factor_gslmod = 1.0;  // scale gslmod
  factor_hfmdc  = 1.0;  // scale hfmdc
  factor_nperm  = 1.0;  // scale nperm
} .flag(); 

group {
N;
P;
XBprer;
XBpostr;
xXBpostr;
xXBprer;
TnCaL;
TnCaH;
simCa;
dtyf_postr;
dtyf_prer;
fappT;
gappT;
hfT;
hbT;
gxbT;
SL;
SOVFThin;
Trop_app_Ca;
Fract_SBXB;
SSXBprer;
SSXBpostr;
} .trace();

xbmod=1*factor_xbmod;            
xbm=1*factor_xbm;
main_k=1.2*1.5*2.0*2.0;

if (paramset==OLIVEIRA) {
   kxb=factor_kxb*35;                     
} else {
   kxb=factor_kxb*70*main_k;
}

/* Sarcomere Geometry */
SLmax=2.4;                        /* um */
SLmin=1.4;                        /* um */
len_thick=1.65;                   /* um */   
len_hbare=0.1;                    /* um */
len_thin=1.2;                     /* um */

/* Temperature Dependence - Define Q10 Values */
RTF=8314.0*(273+37)/96500.0;      /* the value is 26.708 */
TempCelsius=37;                   /* deg C */   
TempKelvin=(273.0+TempCelsius);   /* Kelvin */
Qkon=1.5;                         /* Unitless */
Qkoff=1.3;                        /* Unitless */
Qkn_p=1.6;                        /* Unitless */
Qkp_n=1.6;                        /* Unitless */
Qfapp=6.25;                       /* Unitless */
Qgapp=2.5;                        /* Unitless */
Qhf=6.25;                         /* Unitless */
Qhb=6.25;                         /* Unitless */
Qgxb=6.25;                        /* Unitless */

/* Ca binding to troponin to thin filament regulation */
/* Rates for troponin C - Ca binding                  */
if (paramset==OLIVEIRA) {
	kon=(50.e-3)*0.95*factor_kon;        /* (uM*ms)^-1 */
} else {
	kon=50.e-3*factor_kon;               /* (uM*ms)^-1 */
}

/* Low affinty offrate */
koffL=250.e-3*factor_koffL;                  /* ms^-1 */

/* High affinty offrate */
koffH=25.e-3*factor_koffH;                   /* ms^-1 */

perm50=0.5*factor_perm50; .trace();          /* Unitlesss */

/* Set perm variable that control n to p transiton in Hill-like fashion */
nperm=15.0*factor_nperm;                    /* Unitless */

if (paramset==OLIVEIRA) {
	kn_p=610e-3*factor_kn_p*0.25;       /* ms^-1 */  
    	kp_n=16e-3*factor_kp_n*1.50;        /* ms^-1 */      
} else {
	kn_p=50e-3*factor_kn_p;             /* ms^-1 */
	kp_n=500e-3*factor_kp_n;            /* ms^-1 */
}

/* Thin filament regulation and crossbridge cycling */
/* Set the XB off rate                              */
gapmdc = 0;                                 /* Unitless */
gapmd=exp(gapmdc*(xXBprer/x_0)*(xXBprer/x_0));                

if (paramset==OLIVEIRA) {
    fapp=4800.e-3*xbm*factor_fapp;          /* ms^-1 */     
    gapp=93.0e-3*xbm*gapmd*factor_gapp*2.00;/* ms^-1 */ 
} else {
    fapp=500.e-3*xbm*factor_fapp;           /* ms^-1 */
    gapp=70.0e-3*xbm*gapmd*factor_gapp;     /* ms^-1 */
}
/* # Parameter to control SL effect on gapp, set between 0 and 1 */
gslmod=6*factor_gslmod;                     /* Unitlesss */

/* Set rate for rates between pre-force and force states */
if (paramset==OLIVEIRA) {
    hf=10.0e-3*xbm*factor_hf;               /* ms^-1 */      
} else {
    hf=2000.0e-3*xbm*factor_hf;             /* ms^-1 */
}
hfmdc=5*factor_hfmdc;                       /* Unitless */
if (paramset==OLIVEIRA) {
        hb=34.0e-3*xbm*factor_hb;           /* ms^-1 */     
} else {
	hb=400.0e-3*xbm*factor_hb;          /* ms^-1 */
}
hbmdc=0;
/* Set rate for ATP consuming transition */
if (paramset==OLIVEIRA) {
	gxb=30.e-3*xbm*factor_gxb;           /* ms^-1 */   
} else {
	gxb=70.e-3*xbm*factor_gxb;           /* ms^-1 */
}
gxmdc=60.0;
/* Add term for distortion dependence of STP using transition gxb */
sigmap=8.0;                                  /* Unitless */
sigman=1.0;                                  /* Unitless */

/* Mean strain of strongly-bound states */
// Set strain induced by head rotation
x_0=0.007;                         	    /* um */

/* Set scaling factor that set balance of two competing effects on mean strain - SL motion and XB cycling */
xPsi=2;                                     /* Unitless */

/* Normalized active and passive force */
SLset=2.3;                                  /* um */
SLrest=1.9;                                 /* um */
// These apply to trabeculae and single cells - assume to represent titin
PCon_t= 0.002;                              /* Unit Normalized Force */
PExp_t= 10;                                 /* Unitless */
// These apply to trabeculae only - assume to represent collagen
SLcol= 2.25;                                /* um */
PCon_col= 0.02;                             /* Unit Normalized Force */
PExp_col= 70;                               /* Unitless */

/* Calculation of complete muscle response         */
/* Value is taken from de Tomble & ter Keurs, 1991 */
visc=0.003e3;                      	    /* Unit Normalized force /(um/ms) */

// Code for defining a series elastic element
KSE= 1;                                     /* Normalized Force/um */

/* Initial values for myofilament */
N_NoXB=0.99;                                /* probability */
P_NoXB=0.01;                                /* probability */
N_init=0.97;                                /* probability */
P_init=0.01;                                /* probability */
XBprer_init=0.01;                           /* probability */
XBpostr_init=0.01;                          /* probability */ 
xXBpostr_init=x_0;                          /* um */
xXBprer_init=0.0;                           /* um */  
TnCaL_init=0.01447254;                      /*    */
TnCaH_init=0.2320947;                       /*    */

/* Equations */
diff_TnCaH = konT * simCa * (1.0 - TnCaH) - koffHT * TnCaH;                                // Eq1
diff_TnCaL = konT * simCa * (1.0 - TnCaL) - koffLT * TnCaL;                                // Eq2

/* Handle Ca binding to troponin here */
/*  Adjust for temperature            */
konT=kon*pow(Qkon,(TempCelsius-37)/10.);                                                   // Eq4
koffHT=koffH*xbmod*pow(Qkoff,(TempCelsius-37)/10.);                                        // Eq5
koffLT=koffL*xbmod*pow(Qkoff,(TempCelsius-37)/10.);                                        // Eq6

/* Compute combined Ca binding to high (w/XB) and low (w/o XB)  No Units. */
perm = (1.0 - SOVFThin) * TnCaL + SOVFThin * TnCaH;                                        // Eq9

/* Set perm variable that control n to p transiton in Hill-like fashion, No Units. */
permtot = sqrt((1.0 / (1.0 + pow((perm50 / perm), nperm))));                               // E10

/* Adjust for Ca activation level and temp. */
kn_pT=kn_p*permtot*pow(Qkn_p,(TempCelsius-37.)/10.);                                       // Eq11

if (100.0 < 1.0/permtot) {                                                                 // Eq12
 inprmt=100.0;
} else {
 inprmt=1.0/permtot;
}

kp_nT=kp_n*inprmt*pow(Qkp_n,(TempCelsius-37.)/10.);                                         // Eq13

diff_N = -kn_pT * N + kp_nT * P;                                                            // Eq14
diff_P = -kp_nT * P + kn_pT * N - fappT * P + gappT * XBprer + gxbT * XBpostr;              // Eq15
diff_XBprer = fappT * P - gappT * XBprer - hfT * XBprer + hbT * XBpostr;                    // Eq16
diff_XBpostr = hfT * XBprer - hbT * XBpostr - gxbT * XBpostr;                               // Eq17

/* Compute fapp, the attachment rate from weak to strong, pre-rotated state */
fappT=fapp*xbmod*pow(Qfapp,(TempCelsius-37.)/10.);                                          // Eq18

/* Compute gapp, the detachment rate from strong, pre-rotated to the weakly-bound state */
/* Compute SL modifier for gapp to increase rate st shorter SL                          */
gappT=gapp*gapslmd*xbmod*pow(Qgapp,(TempCelsius-37)/10.);                                   // Eq19
gapslmd = 1.0 + (1.0 - SOVFThick)*gslmod;                                                   // Eq20

if (paramset==MEL_ORIGINAL) {
	gappT*=exp(fabs(pow(xXBprer/x_0*20,4)));
}

/* Combine modifiers of hf */
hfT=hf*hfmd*xbmod*pow(Qhf,(TempCelsius-37)/10.);                                             // Eq21

/* Set rate for rates between pre-force and force states */
/* Compute modifiers based on mean strain of states.     */
   hfmd=exp(-sign(xXBprer)*hfmdc*((xXBprer/x_0)*(xXBprer/x_0)));                             // Eq22

/* Combine modifiers of hb */
hbT=hb*xbmod*pow(Qhb,(TempCelsius-37)/10.);                                                  // Eq23

/* Set rate for rates gxb, the ATP using XB transition */
/* Add term for distortion dependence of gxb */                                              // Eq24
if (x_0-xXBpostr>0) {
	gxbmd=exp(sigmap*pow((x_0-xXBpostr)/x_0,2));
} else {
	gxbmd=exp(sigman*pow((x_0-xXBpostr)/x_0,2));
}

gxbT=gxb*gxbmd*xbmod*pow(Qgxb,(TempCelsius-37.)/10.);                                        // Eq25

riceYear == RICE2008
xGatexp1=0;
xGatexp2=0;
xGatetau2=0;
diff_xXBprer = dSL/2. + xPsi/dtyf_prer*(-xXBprer*fappT+(xXBpostr-x_0-xXBprer)*hbT);          // Eq29
diff_xXBpostr = dSL/2. + xPsi/dtyf_postr*((x_0+xXBprer-xXBpostr));                           // Eq30

// Compute the duty fractions
// Compute for states XBpref and XBf
dtyf_prer=(hbT*fappT+gxbT*fappT)/(fappT*hfT+gxbT*hfT+gxbT*gappT+hbT*fappT+hbT*gappT+gxbT*fappT);      // Eq31
dtyf_postr=fappT/(fappT*hfT+gxbT*hfT+gxbT*gappT+hbT*fappT+hbT*gappT+gxbT*fappT);                      // Eq32

SSXBprer = (hb*fapp+gxb*fapp)/(gxb*hf+fapp*hf+gxb*gapp+hb*fapp+hb*gapp+gxb*fapp);                     // Eq33
SSXBpostr = fapp*hf/(gxb*hf+fapp*hf+gxb*gapp+hb*fapp+hb*gapp+gxb*fapp);                               // Eq34

force_coeff=1.0/x_0/SSXBpostr;                                                                        // Eq35 part 1
force_from_rice=SOVFThick*(xXBpostr*XBpostr+xXBprer*XBprer)*force_coeff;                              // Eq35 part 2
force_from_S=kxb*force_from_rice;
tension=force_from_S;

// Compute the fraction of strongly-bound crossbridges 
Fract_SBXB_init = (XBprer_init+XBpostr_init)/(SSXBprer+SSXBpostr);                                    // Eq36

// Compute apparent Ca binding to Troponin                                                            // Eq37
Trop_app_init = (1-SOVFThin_init)*TnCaL_init+SOVFThin_init*(Fract_SBXB_init*TnCaH_init+(1-Fract_SBXB_init)*TnCaL_init);

// Compute z-line end of single overlap region
if (len_thick < rl) {                                                                                 // Eq42
  sovr_ze_init = len_thick/2;
} else {
  sovr_ze_init = rl/2;
}

// Compute centerline of end of single overlap region
sovr_cle_intermediate = rl/2.-(rl-len_thin);                                                          // Eq43
if (sovr_cle_intermediate > len_hbare/2) {
  sovr_cle_init = sovr_cle_intermediate;
} else {
  sovr_cle_init = len_hbare/2;
}

/* Compute length of the single overlap */
len_sovr_init=sovr_ze_init-sovr_cle_init;                                                             // Eq44

/* Compute overlap fraction for thick filament */
SOVFThick_init = len_sovr_init*2./(len_thick-len_hbare);                                              // Eq45

/* Compute overlap fraction for thin filament */
SOVFThin_init = len_sovr_init/len_thin;                                                               // Eq46

// Compute Ca binding to Troponin
Trop_conc= 70.0/1000;                                                                                  // uM -> mM 
Trop_app_Ca_init = Trop_conc*Trop_app_init;                                                            // Eq56
diff_Trop_app_Ca = Trop_conc*diff_Trop_app;                                                            // Eq57
diff_Trop_app = -diff_SOVFThin*TnCaL+(1-SOVFThin)*diff_TnCaL+diff_SOVFThin*(Fract_SBXB*TnCaH+(1-Fract_SBXB)*TnCaL)+SOVFThin*(diff_Fract_SBXB*TnCaH+Fract_SBXB*diff_TnCaH-diff_Fract_SBXB*TnCaL+(1-Fract_SBXB)*diff_TnCaL);                                                                        // Eq58

if (rl < len_thick) {                                                                                  // Eq59
  diff_sovr_ze = -dSL/2;
} else {
  diff_sovr_ze = 0;
}

if ( (2*len_thin-rl) > len_hbare) {                                                                    // Eq60
  diff_sovr_cle = -dSL/2; 
} else {
  diff_sovr_cle = 0;
}

diff_len_sovr = diff_sovr_ze-diff_sovr_cle;                                                            // Eq61
diff_SOVFThick = (2*diff_len_sovr)/(len_thick-len_hbare);                                              // Eq62   
diff_SOVFThin = diff_len_sovr/len_thin;                                                                // Eq63
diff_Fract_SBXB = (diff_XBprer+diff_XBpostr)/(SSXBprer+SSXBpostr);                                     // Eq64

simCa = Cai*1000;                                                                                      // mM -> uM
SL=SLrest*lambda;
realLength=lambda*SLrest;
rl=realLength;
dSL=stretchVel*SLrest/2.0;